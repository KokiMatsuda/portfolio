﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲームオブジェクトのFadeIn/Out処理
/// </summary>
public class Fade : SingletonMonoBehaviour<Fade>
{
    /// <summary>
    /// ゲームオブジェクトのフェードイン
    /// </summary>
    /// <param name="FadeInObj">フェードインするゲームオブジェクト</param>
    /// <param name="MaxAlpha">最終的なアルファ値</param>
    public void FadeIn(GameObject FadeInObj, float MaxAlpha)
    {
        StartCoroutine(FadeInCol(FadeInObj, MaxAlpha));
    }

    /// <summary>
    /// ゲームオブジェクトのフェードアウト
    /// </summary>
    /// <param name="FadeOutObj">フェードアウトするゲームオブジェクト</param>
    public void FadeOut(GameObject FadeOutObj)
    {
        StartCoroutine(FadeOutCol(FadeOutObj));
    }

    /// <summary>
    /// ゲームオブジェクトのフェードイン処理
    /// </summary>
    /// <param name="FadeInObj">フェードインするゲームオブジェクト</param>
    /// <param name="MaxAlpha">最終的なアルファ値</param>
    public IEnumerator FadeInCol(GameObject FadeInObj, float MaxAlpha)
    {
        //現在のアルファ値
        float Alpha = FadeInObj.GetComponent<SpriteRenderer>().color.a;

        while (Alpha < MaxAlpha)
        {
            Alpha += 0.1f;
            FadeInObj.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, Alpha);

            //1フレーム待つ
            yield return null;
        }
    }

    /// <summary>
    /// ゲームオブジェクトのフェードアウト処理
    /// </summary>
    /// <param name="FadeOutObj">フェードアウトするゲームオブジェクト</param>
    public IEnumerator FadeOutCol(GameObject FadeOutObj)
    {
        //Color color = FadeInObj.GetComponent<SpriteRenderer>().color;
        float Alpha = FadeOutObj.GetComponent<SpriteRenderer>().color.a;

        while (Alpha > 0)
        {
            Alpha -= 0.1f;
            FadeOutObj.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, Alpha);

            //1フレーム待つ
            yield return null;
        }
    }
}