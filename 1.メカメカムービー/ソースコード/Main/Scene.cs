﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// シーン遷移
/// </summary>
public class Scene : SingletonMonoBehaviour<Scene>
{

    //　フェードインのおおよその秒数
    [SerializeField]
    float fadeTime;
    //　背景Image
    public Image imageFadeIn;
    //　背景Image
    public Image imageFadeOut;

    //FadeInの処理が終わるとtrue
    public static bool FadeInEnd = false;

    void Start()
    {
        //　コルーチンで使用する待ち時間を計測
        fadeTime = 1f * fadeTime / 10f;
        StartCoroutine(FadeIn());

        //FadeOutパネルの当たり判定(Ray判定)をfalseにする
        imageFadeIn.raycastTarget = false;
    }


    //タイトルへ遷移するボタン用(Stageシーン、Resultシーン)
    public void ToTitle()
    {
        StartCoroutine(Fadeout("Title"));
        //SceneManager.LoadScene("Title");
    }

    //ゲーム開始ボタン用(Titleシーン、Resultシーン)
    public void GameStart()
    {
        //体力を初期化
        GameManager.m_HP = 3;
        //スコアを削除
        GameManager.m_Score = 0;
        //ステージ番号を初期化
        GameManager.m_StageNum = 0;
        StartCoroutine(Fadeout("Stage"));
        //SceneManager.LoadScene("Stage");
    }

    //ゲーム続行ボタン用(Stageシーン)
    public void NextGame()
    {
        Debug.Log("NextStage");
        //次のステージ番号
        GameManager.m_StageNum++;
        StartCoroutine(Fadeout("Stage"));
        //SceneManager.LoadScene("Stage");
    }

    //Resultシーンへ遷移する用
    public void ToResult()
    {
        StartCoroutine(Fadeout("Result"));
        //SceneManager.LoadScene("Result");
    }


    IEnumerator FadeIn()
    {
        //　Colorのアルファを0.1ずつ下げていく
        for (float i = 1f; i >= 0; i -= 0.1f)
        {
            imageFadeIn.color = new Color(0f, 0f, 0f, i);
            //　指定秒数待つ
            yield return new WaitForSeconds(fadeTime);
        }
        imageFadeIn.color = new Color(0f, 0f, 0f, 0);
        FadeInEnd = true;

        ////現在シーン名が"Stage"ならゲーム開始関数呼び出し
        //if (SceneManager.GetActiveScene().name == "Stage")
        //{
        //    GameManager.Instance.NextMode();
        //}
    }

    IEnumerator Fadeout(string SceneName)
    {
        //FadeOutパネルの当たり判定(Ray判定)をtrueにする
        imageFadeIn.raycastTarget = true;

        //　Colorのアルファを0.1ずつ上げていく
        for (float i = 0f; i <= 1; i += 0.1f)
        {
            imageFadeOut.color = new Color(0f, 0f, 0f, i);
            //　指定秒数待つ
            yield return new WaitForSeconds(fadeTime);
        }
        imageFadeIn.color = new Color(0f, 0f, 0f, 1);
        FadeInEnd = false;

        SceneManager.LoadScene(SceneName);
    }
}
