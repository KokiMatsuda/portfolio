﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// スコア表示
/// </summary>
public class ScoreText : SingletonMonoBehaviour<ScoreText> {

    //スコア表示用
    public Text scoreText;

    //シーン遷移後と呼び出されたとき実行
    public void Start()
    {
        //スコア表示
        scoreText.text = GameManager.m_Score.ToString();
    }
}
