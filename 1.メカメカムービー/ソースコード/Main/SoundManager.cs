﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonMonoBehaviour<SoundManager> {

    public AudioSource SE;
    public AudioSource TapSE;

    public void Tap()
    {
        Debug.Log("TapSE");
        TapSE.Play();
    }
}
