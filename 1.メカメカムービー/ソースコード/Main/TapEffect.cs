﻿using UnityEngine;


/*タップした場所にパーティクルを発生させる*/
public class TapEffect : MonoBehaviour
{
    // カメラの座標
    [SerializeField]
    Camera _camera;                        

    // タップエフェクト
    [SerializeField]
    ParticleSystem tapEffect;

    //タップサウンド
    public AudioSource TapSE;

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            // マウスのワールド座標までパーティクルを移動し、パーティクルエフェクトを1つ生成する

            var pos = _camera.ScreenToWorldPoint(Input.mousePosition + _camera.transform.forward * 10);

            tapEffect.transform.position = pos;
            
            tapEffect.Emit(1);

            //タップサウンド再生
            TapSE.Play();
        }
    }
}