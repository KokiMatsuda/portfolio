﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Playerタグに当たったら爆発ゲームオブジェクトを表示
public class BombExplosion : MonoBehaviour {

    public string PlayerTag = "Player";
    public GameObject ExplosionObj;


    void Start()
    {
        ExplosionObj.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //当たったコライダーのタグがプレイヤーだった場合
        if (collision.tag == PlayerTag)
        {
            ExplosionObj.SetActive(true);
        }
    }
}
