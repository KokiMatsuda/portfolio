﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// スタートアニメーション(Canvas/START)
/// </summary>
public class Countdown : MonoBehaviour {

    public AudioSource CountSound;
    public AudioSource GoSound;

    public void ChangeText(string msg)
    {
        GetComponentInChildren<Text>().text = msg;
    }

    public void GameStart()
    {
        GameManager.Instance.NextMode();
    }

    public void PlayCountSound()
    {
        CountSound.Play();
    }

    public void PlayGoSound()
    {
        GoSound.Play();
    }
}
