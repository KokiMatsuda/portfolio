﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ボタンUIが押されたときの処理(GameManager)
/// </summary>
public class FlagGroup : SingletonMonoBehaviour<FlagGroup> {

    #region//宣言

    [Tooltip("タイムスライダ―(GM2)")]
    public Slider timeSlider;

    [Tooltip("時間表示用")]
    public Text timeText;

    //旗の数
    public const int MaxFlag = 3;

    /// <summary>
    /// 旗の構造体
    /// </summary>
    public struct FlagContent
    {
        //旗の種類
        public int FlagTypeID;
        
        //旗の場所
        public int FlagPosID;
    }

    /// <summary>
    /// タイムスライダーの秒数(データ格納用)
    /// </summary>
    public FlagContent[] SliderTime = new FlagContent[TimeLine.MaxTime + 1];

    //同じ場所に置かれているか判断用(同じ場所(5箇所+初期ポジション1)に置かれている旗の数　0～3個)
    public int[] NumOfFlags = new int[6];

    //0～2の3種類の旗ID
    public Stack<int> StackFlagID = new Stack<int>();
    
    [Tooltip("旗(3種類)")]
    public GameObject[] FlagObj = new GameObject[MaxFlag];

    [Tooltip("旗(オブジェクト)の初期ポジション")]
    public Vector2[] FlagObjFirstPos = new Vector2[MaxFlag];
    
    [Tooltip("旗UI(3種類)")]
    public GameObject[] FlagUIObj;
    
    [Tooltip("旗UI用タイムライン上のポジション左端")]
    public Transform UIPosA;
    [Tooltip("旗UI用タイムライン上のポジション右端")]
    public Transform UIPosB;

    #endregion

    void Start () {

        //時間表示
        timeText.text = ("00" + "       " + "\n / " + TimeLine.MaxTime.ToString());

        //スタックを3つ作成(IDを格納{0,1,2})
        StackFlagID.Push(0);
        StackFlagID.Push(1);
        StackFlagID.Push(2);


        //FlagIDとFlagPosに-1を格納
        for (int i = 0; i < SliderTime.Length; i++)
        {
            SliderTime[i].FlagTypeID = -1;
            SliderTime[i].FlagPosID = -1;
        }
    }



    /// <summary>
    ///画面上のボタンが押されたとき呼ばれる
    /// </summary>
    public void FlagProcessing(int FlagPosID)
    {

        int TS_val = (int)timeSlider.value;

        //選択した時間"SliderTime"を"SliderT"に省略
        var SliderT = SliderTime[TS_val];

        //指定した場所にすでに旗が置いてあった場合はその旗を消す
        //(選択した時間のFlagTypeIDに-1以外の数値が入っていた場合)
        if (SliderT.FlagTypeID != -1)
        {
            int FlagCount = StackFlagID.Count;

            //StackFlagIDに選択した時間においてある旗をPush
            StackFlagID.Push(SliderT.FlagTypeID);


            //選択した時間に置いてあった旗をもとのポジションに戻す(旗UIは消す)
            FlagObj[SliderT.FlagTypeID].transform.position = FlagObjFirstPos[FlagCount];
            FlagUIObj[SliderT.FlagTypeID].SetActive(false);


            //指定した場所の旗の数を1引く
            NumOfFlags[SliderT.FlagPosID] += -1;

            //置いてあった場所のIDを-1にする(初期化)
            SliderT.FlagTypeID = -1;
            SliderT.FlagPosID = -1;

        }
        //スタックしてあるFlagの数が0より多い場合
        else if (StackFlagID.Count > 0)
        {
            //PopしたFlagのID(2→1→0)
            int PopNum = StackFlagID.Pop();

            //選択したタイムスライダーの秒数目のFlagTypeIDにポップした番号IDを格納
            SliderT.FlagTypeID = PopNum;

            //選択したタイムスライダーの秒数目のFlagPosIDにポップしたFlagの場所のIDを格納
            SliderT.FlagPosID = FlagPosID;

            //旗が被らないようにする(同じ場所に何個旗があるか判断)
            switch (NumOfFlags[SliderT.FlagPosID])
            {
                case 1://同じ場所に旗が1個→X座標を右に0.5ずらす
                    //ポップしたFlagのIDと同じインデックス番号のゲームオブジェクトをその場所に移動させる
                    FlagObj[PopNum].transform.position
                        = new Vector3((GameManager.m_SpownPos[SliderT.FlagPosID]) + 0.5f, GameManager.m_GROUND, 0);
                    break;

                case 2://同じ場所に旗が2個→X座標を左に0.5ずらす
                    FlagObj[PopNum].transform.position
                        = new Vector3((GameManager.m_SpownPos[SliderT.FlagPosID]) - 0.5f, GameManager.m_GROUND, 0);
                    break;

                default:
                    FlagObj[PopNum].transform.position
                        = new Vector3((GameManager.m_SpownPos[SliderT.FlagPosID]), GameManager.m_GROUND, 0);
                    break;
            }

            Debug.Log(FlagObj[PopNum].transform.position);

            //ポップしたFlagのIDと同じインデックス番号の旗UIを指定したタイムライン上に移動＆有効化
            //FlagUIObj[PopNum].transform.position = FlagUIObjPos[TS_val];
            FlagUIObj[PopNum].transform.position = Vector3.Lerp(UIPosA.position, UIPosB.position, TS_val / 10.0f);
            FlagUIObj[PopNum].SetActive(true);

            //指定した場所の旗の数を1足す
            NumOfFlags[SliderT.FlagPosID] += 1;
        }
        else
        {
            Debug.Log("旗の数が足りない");
        }

        //コピー元にデータを戻す
        SliderTime[TS_val] = SliderT;
    }


    public void FlagTimeText()
    {
        //時間表示
        timeText.text = (((int)timeSlider.value).ToString() + "       " + "\n / " + TimeLine.MaxTime.ToString());
    }

    /// <summary>
    /// FlagObjの初期位置登録
    /// </summary>
    public void FlagObjFirstPosition()
    {
        //旗(オブジェクト)の初期ポジション格納
        for (int i = 0; i < MaxFlag; i++)
        {
            FlagObjFirstPos[i] = FlagObj[i].transform.position;
            //Debug.Log(FlagObjFirstPos[i]);
        }
    }

    /// <summary>
    /// FlagGroupの消失アニメーション(選択されていないもののみ)
    /// </summary>
    public void FlagAnimation()
    {
        for (int i = 0; i < MaxFlag; i++)
        {
            //UIがアクティブでないなら対応するFlagObjを消去アニメーション
            if (FlagUIObj[i].activeSelf == false)
            {
                Debug.Log(FlagUIObj[i].name);
                Debug.Log(GameManager.Instance.m_GM2.m_FlagObj[i].name);
                //FlagObjのアニメーション
                GameManager.Instance.m_GM2.m_FlagObj[i].GetComponent<Animator>().SetBool("Apper", false);
            }
        }
    }
  
}
