﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

/// <summary>
/// モード変更、出現X軸操作、スコア計算(GameManager)
/// </summary>
public class GameManager : SingletonMonoBehaviour<GameManager>
{
    
    //生成させるX軸設定用
    public static readonly int[] m_SpownPos = new int[] { -6, -3, 0, 3, 6 };

    //地面の高さ
    public const float m_GROUND = -2.0f;

    [Header("<PlayerInfo>")]
    [Tooltip("Player")]
    public GameObject m_PlayerChar;

    //プレイヤーの初期位置(画面外)
    public Vector2 PlayerFadeInPos;

    public static int m_HP = 3;

    [Tooltip("スコア")]
    public static int m_Score;

    [Tooltip("得点")]
    public const int m_PlusScore = 10;


    [Space(10)]
    [Header("<GameMode>"), Range(-1, 4)]//ゲームモード変更用
    public int m_GameMode;

    [Tooltip("カウントダウンアニメーション")]
    public GameMode0 m_GM0;

    [Tooltip("見て覚えるモード")]
    public GameMode1 m_GM1;

    [Tooltip("キャラクターを操作するモード")]
    public GameMode2 m_GM2;

    [Tooltip("実行するモード")]
    public GameMode3 m_GM3;

    [Tooltip("次のステージに進むか選択するモード")]
    public GameMode4 m_GM4;


    [Space(10)]
    [Tooltip("ステージ番号")]
    public static int m_StageNum = 0;
    

    void Start()
    {
        m_GameMode = -1;
        #region//全て非表示
        //Playerのスクリプトと当たり判定無効化
        m_PlayerChar.GetComponent<Player>().enabled = false;
        m_PlayerChar.GetComponent<BoxCollider2D>().enabled = false;

        AllSetActive(false,
            m_GM0.m_StartAnim,

            m_GM1.m_TimeLine,

            m_GM2.m_TimeSlider,
            m_GM2.m_FlagUIColGroup,
            m_GM2.m_NextModeButton,
            m_GM2.m_RePlayButton,

            m_GM4.m_NextStageButton,
            m_GM4.m_ToTitleButton
            );

        //プレイヤーを画面外に出す
        m_PlayerChar.transform.position = PlayerFadeInPos;

        #endregion


    }


    /// <summary>
    /// 疑似シーン遷移用(0～3モード)
    /// </summary>
    public void NextMode()
    {
        m_GameMode++;

        switch (m_GameMode)
        {
            case 0://Startのアニメーション
                #region//不要
                //Playerのスクリプトと当たり判定無効化
                m_PlayerChar.GetComponent<Player>().enabled = false;
                m_PlayerChar.GetComponent<BoxCollider2D>().enabled = false;

                AllSetActive(false,
                    m_GM1.m_TimeLine,

                    m_GM2.m_TimeSlider,
                    m_GM2.m_FlagUIColGroup,
                    m_GM2.m_NextModeButton,
                    m_GM2.m_RePlayButton,
                    
                    m_GM4.m_NextStageButton,
                    m_GM4.m_ToTitleButton
                    );

                //プレイヤーを画面外に出す
                m_PlayerChar.transform.position = PlayerFadeInPos;

                #endregion


                #region//必要
                //スコアテキストを表示して縮小
                m_GM4.m_ScoreTextGroup.SetActive(true);
                //m_GM4.m_ScoreTextGroup.GetComponent<Animator>().SetBool("Big", true);//一度拡大
                //m_GM4.m_ScoreTextGroup.GetComponent<Animator>().SetBool("BecomeExpansion", false);//縮小

                //Startカウントダウン用のオブジェクトを有効化・アニメーション実行
                m_GM0.m_StartAnim.SetActive(true);
                m_GM0.m_StartAnim.GetComponentInChildren<Animator>().SetBool("Start", true);
                #endregion
                break;


            case 1://"見て覚える"シーン
                #region//不要
                //Playerのスクリプトと当たり判定無効化
                m_PlayerChar.GetComponent<Player>().enabled = false;
                m_PlayerChar.GetComponent<BoxCollider2D>().enabled = false;

                AllSetActive(false,
                m_GM0.m_StartAnim,

                m_GM2.m_TimeSlider,
                m_GM2.m_FlagUIColGroup,
                m_GM2.m_NextModeButton,
                m_GM2.m_RePlayButton,
                
                m_GM4.m_NextStageButton,
                m_GM4.m_ToTitleButton
                    );

                //プレイヤーを画面外に出す
                m_PlayerChar.transform.position = PlayerFadeInPos;
                #endregion


                #region//必要
                //最後に見たインデックス番号を初期化
                SpownSystem.Instance.Header = 0;

                //タイムラインを有効化・実行
                m_GM1.m_TimeLine.SetActive(true);
                TimeLine.Instance.TimeStart();

                //スコアテキストを小さい状態で表示
                m_GM4.m_ScoreTextGroup.SetActive(true);
                m_GM4.m_ScoreTextGroup.GetComponent<Animator>().SetBool("Big", false);
                #endregion
                break;


            case 2://"キャラクターの移動位置を設定する"シーン
                #region//不要
                //Playerのスクリプトと当たり判定無効化
                m_PlayerChar.GetComponent<Player>().enabled = false;
                m_PlayerChar.GetComponent<BoxCollider2D>().enabled = false;

                AllSetActive(false,
                m_GM0.m_StartAnim,

                m_GM1.m_TimeLine,
                
                m_GM4.m_NextStageButton,
                m_GM4.m_ToTitleButton
                    );

                //Itemタグのゲームオブジェクトを消す
                GameObject[] Item_Good = GameObject.FindGameObjectsWithTag("Item_Good");
                GameObject[] Item_Bad = GameObject.FindGameObjectsWithTag("Item_Bad");
                for (int i = 0; i < Item_Good.Length; i++)
                {
                    Destroy(Item_Good[i]);
                }
                for (int i = 0; i < Item_Bad.Length; i++)
                {
                    Destroy(Item_Bad[i]);
                }

                //プレイヤーを画面内に移動させる(X:SpownPos[0](左端)　Y:Ground)
                m_PlayerChar.GetComponent<Rigidbody2D>().MovePosition(new Vector2(m_SpownPos[0], m_GROUND));
                #endregion


                #region//必要

                AllSetActive(true,
                m_GM2.m_TimeSlider,
                m_GM2.m_FlagObjGroupPanel,
                m_GM2.m_FlagObj[0],
                m_GM2.m_FlagObj[1],
                m_GM2.m_FlagObj[2],
                m_GM2.m_FlagUIColGroup,
                m_GM2.m_NextModeButton,
                m_GM2.m_RePlayButton
                    );

                
                //FlagObjGroupの出現アニメーション
                m_GM2.m_FlagObjGroupPanel.GetComponent<Animator>().SetBool("Apper", true);
                m_GM2.m_FlagObj[0].GetComponent<Animator>().SetBool("Apper", true);
                m_GM2.m_FlagObj[1].GetComponent<Animator>().SetBool("Apper", true);
                m_GM2.m_FlagObj[2].GetComponent<Animator>().SetBool("Apper", true);

                //FlagObjの初期位置登録
                FlagGroup.Instance.FlagObjFirstPosition();

                //スコアテキストを小さい状態で表示
                m_GM4.m_ScoreTextGroup.SetActive(true);
                m_GM4.m_ScoreTextGroup.GetComponent<Animator>().SetBool("Big", false);
                #endregion
                break;


            case 3://"実行する"シーン
                #region//不要
                AllSetActive(false,
                m_GM0.m_StartAnim,

                m_GM2.m_TimeSlider,
                m_GM2.m_FlagUIColGroup,
                m_GM2.m_NextModeButton,
                m_GM2.m_RePlayButton,
                
                m_GM4.m_NextStageButton,
                m_GM4.m_ToTitleButton
                    );

                //FlagObjGroupと選択されていないFlagObjの消失アニメーション
                m_GM2.m_FlagObjGroupPanel.GetComponent<Animator>().SetBool("Apper", false);
                FlagGroup.Instance.FlagAnimation();
                #endregion


                #region//必要
                //Playerのスクリプトと当たり判定有効化
                m_PlayerChar.GetComponent<Player>().enabled = true;
                m_PlayerChar.GetComponent<BoxCollider2D>().enabled = true;

                //最後に見たインデックス番号を初期化
                SpownSystem.Instance.Header = 0;

                //タイムラインを有効化・実行
                m_GM1.m_TimeLine.SetActive(true);
                TimeLine.Instance.TimeStart();
                
                m_PlayerChar.GetComponent<Player>().ColStart();

                //スコアテキストを小さい状態で表示
                m_GM4.m_ScoreTextGroup.SetActive(true);
                m_GM4.m_ScoreTextGroup.GetComponent<Animator>().SetBool("Big", false);
                #endregion
                break;


            case 4://続行するか(できるか)判断
                //HPが0だとResultシーンへ遷移
                if (m_HP <= 0)
                {
                    SceneManager.LoadScene("Result");
                }

                #region//不要
                //Playerのスクリプトと当たり判定無効化
                m_PlayerChar.GetComponent<Player>().enabled = false;
                m_PlayerChar.GetComponent<BoxCollider2D>().enabled = false;

                AllSetActive(false,
                m_GM0.m_StartAnim,

                m_GM1.m_TimeLine,

                m_GM2.m_TimeSlider,
                m_GM2.m_FlagUIColGroup,
                m_GM2.m_NextModeButton,
                m_GM2.m_RePlayButton
                    );

                //Itemタグのゲームオブジェクトを消す
                Item_Good = GameObject.FindGameObjectsWithTag("Item_Good");
                Item_Bad = GameObject.FindGameObjectsWithTag("Item_Bad");
                for (int i = 0; i < Item_Good.Length; i++)
                {
                    Destroy(Item_Good[i]);
                }
                for (int i = 0; i < Item_Bad.Length; i++)
                {
                    Destroy(Item_Bad[i]);
                }
                #endregion


                #region//必要
                AllSetActive(true,
                m_GM4.m_ScoreTextGroup,
                m_GM4.m_NextStageButton,
                m_GM4.m_ToTitleButton
                    );
                //スコアテキストを拡大

                m_GM4.m_ScoreTextGroup.GetComponent<Animator>().SetBool("BecomeExpansion", true);
                //ScoreText.Instance.Start();

                #endregion
                break;


            default:
                SceneManager.LoadScene("Title");
                m_GameMode = 0;
                break;
        }
    }

    /// <summary>
    /// SetActive一括変更
    /// </summary>
    /// <param name="Set">"true" or "false"</param>
    /// <param name="gameObject">SetActiveを変更したいオブジェクト</param>
    public void AllSetActive(bool Set, params GameObject[] gameObject)
    {
        for (int i = 0; i < gameObject.Length; i++)
        {
            gameObject[i].SetActive(Set);
        }
    }

    /// <summary>
    /// もう一度見て覚えるシーンを再生
    /// </summary>
    public void RePlay()
    {
        m_GameMode = 0;
        NextMode();
    }
}



#region//GameMode関数定義
[System.Serializable]
public class GameMode0
{
    public GameObject m_StartAnim;
}

[System.Serializable]
public class GameMode1
{
    public GameObject m_TimeLine;
}

[System.Serializable]
public class GameMode2
{
    public GameObject m_FlagObjGroupPanel;
    public GameObject[] m_FlagObj;
    public Vector2 m_FlagObjGroupPos;
    public GameObject m_FlagUIColGroup;
    public GameObject m_TimeSlider;
    public GameObject m_NextModeButton;
    public GameObject m_RePlayButton;
}

[System.Serializable]
public class GameMode3
{

}

[System.Serializable]
public class GameMode4
{
    public GameObject m_ScoreTextGroup;
    public GameObject m_NextStageButton;
    public GameObject m_ToTitleButton;
}
#endregion