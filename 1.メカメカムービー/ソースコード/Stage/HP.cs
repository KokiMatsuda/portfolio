﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;

/// <summary>
/// 体力の画像表示
/// </summary>
public class HP : SingletonMonoBehaviour<HP>
{

    [Tooltip("体力の画像"), SerializeField]
    GameObject[] HpUI = new GameObject[3];

    [Tooltip("HpUI[0]の初期位置"), SerializeField]
    Vector2 HpUIPos;

    [Tooltip("UIの間隔")]
    public int HpSpace;

    //[Tooltip("体力の画像のanimator")]
    //public Animator HpAnimator;

    

    void Start()
    {
        //HpUIを残り体力分等間隔に並べる
        switch(GameManager.m_HP)
        {
            case 1:
                HpUI[0].transform.localPosition = HpUIPos;
                HpUI[1].GetComponent<Image>().enabled = false;
                HpUI[2].GetComponent<Image>().enabled = false;
                break;

            case 2:
                HpUI[0].transform.localPosition = HpUIPos;
                HpUI[1].transform.localPosition = new Vector2(HpUIPos.x + HpSpace, HpUIPos.y);
                HpUI[2].GetComponent<Image>().enabled = false;
                break;

            case 3:
                HpUI[0].transform.localPosition = HpUIPos;
                HpUI[1].transform.localPosition = new Vector2(HpUIPos.x + HpSpace, HpUIPos.y);
                HpUI[2].transform.localPosition = new Vector2(HpUIPos.x + HpSpace * 2, HpUIPos.y);
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// 体力を1ずつ減らす
    /// </summary>
    public void Damage()
    {
        if (GameManager.m_HP > 0)
        {//現在の体力数から1引いた数が消すべき画像のインデックス番号
            GameManager.m_HP -= 1;
            HpUI[GameManager.m_HP].GetComponent<Image>().enabled = false;

            //HpUI[GameManager.m_HP].GetComponent<Animator>().SetInteger("HP", GameManager.m_HP);

            ////HPが2以上あるなら2つ目の体力のアニメーションも操作
            //for(int i = 0;i<GameManager.m_HP;i++)
            //{
            //    if(HpUI[i].activeSelf)
            //    {
            //        HpUI[i].GetComponent<Animator>().SetInteger("HP", GameManager.m_HP);
            //    }
            //}
        }
    }
}