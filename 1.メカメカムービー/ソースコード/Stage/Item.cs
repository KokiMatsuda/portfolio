﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 出現時間と落下処理(appleとハチの巣)
/// </summary>
public class Item : MonoBehaviour
{
    #region//変数宣言

    public AudioSource LandingSound;

    Rigidbody2D m_rig2D;

    //生成時間、着地時間
    int m_SpownTime = 0;
    int m_LandingTime = 0;

    //落下開始高さ
    float m_SpownHeight = 0;

    //落下時間
    float m_FallingTime = 0;

    //生成されてからの経過時間
    float m_ElapsedTime = 0;

    //落下する高さに対して、今いる高さの割合(高さの率)
    float m_HeightPercentage;

    //このオブジェクトが今いるべき高さ(想定される高さ)
    float m_AssumedHeight = 0;

    //public float m_FadeOutTime;

    //着地したか判定
    bool m_isLanding = false;
    #endregion

    void Start()
    {
        m_rig2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        //経過時間がマイナス値になるのを防ぐ
        if (TimeLine.Instance.time < m_SpownTime)
        {
            return;
        }

        //生成されてからの経過時間 = 現在の時間 - 生成時間
        m_ElapsedTime = TimeLine.Instance.time - m_SpownTime;

        //経過時間が落下時間以上なら消す
        if (m_ElapsedTime >= m_FallingTime + 1 || m_FallingTime == TimeLine.MaxTime)
        {
            Destroy(gameObject);
        }

        //今いるべき高さの割合 = 生成してからの経過時間 / 落下にかかる時間
        m_HeightPercentage = m_ElapsedTime / m_FallingTime;

        //今いるべき高さ
        //Mathf.Lerp(上の高さ, 下の高さ, 割合)
        m_AssumedHeight = Mathf.Lerp(m_SpownHeight, GameManager.m_GROUND, m_HeightPercentage);

        //いるべき高さにいなかったらそこへ移動(MovePostionを使うと移動中の当たり判定も考慮される)
        if (gameObject.transform.position.y != m_AssumedHeight)
        {
            Vector2 vector2 = new Vector2(transform.position.x, m_AssumedHeight);
            m_rig2D.MovePosition(vector2);
            //transform.Postion = new Vector3(transform.Postion.x, AssumedHeight, transform.Postion.z);
        }
        ////地面についたら着地サウンドを再生
        else if (gameObject.transform.position.y == GameManager.m_GROUND && m_isLanding == false)
        {
            m_isLanding = true;
            LandingSound.Play();
            //StartCoroutine(ItemFadeOut());
        }
    }

    /// <summary>
    /// SpownSystemからの値受け取り＆落下時間計算
    /// </summary>
    　/// <param name="spownTime">生成される時間</param>
    /// <param name="landingTime">着地する時間</param>
    public void Receive(int spownTime, int landingTime)
    {
        m_SpownTime = spownTime;
        m_LandingTime = landingTime;

        //SpownSystemにある高さの定数読みこみ
        m_SpownHeight = SpownSystem.SpownHeight;

        //落下時間計算
        m_FallingTime = m_LandingTime - m_SpownTime;
    }

    //IEnumerator ItemFadeOut()
    //{
    //    //透明具合
    //    float fadeOutValue;

    //    Color alphaVal = gameObject.GetComponent<SpriteRenderer>().color;

    //    //スポーンしてからの経過時間が落下にかかる時間 + 1秒以下か、
    //    //落下にかかる時間が最大時間より少ない場合
    //    while (m_ElapsedTime <= m_FallingTime + 1 || m_FallingTime <= TimeLine.MaxTime)
    //    {
    //        今の時間 - 着地した時間
    //        fadeOutValue = Mathf.Lerp(1, 0, m_ElapsedTime - m_FallingTime);
    //        alphaVal = new Color(1, 1, 1, fadeOutValue);
    //        Debug.Log(fadeOutValue);
    //        yield return null;
    //    }

    //    Destroy(gameObject);
    //}
}
