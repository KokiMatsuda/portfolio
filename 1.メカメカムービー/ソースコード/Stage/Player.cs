﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mode3(実行モード)でのFlag追従(Player)
/// </summary>
public class Player : MonoBehaviour
{

    Rigidbody2D m_rig2D;

    public ParticleSystem m_GoodParticle;

    //爆発パーティクルの格納されているゲームオブジェクト
    public GameObject Fire_Explosion;

    //ロボットのmesh
    public GameObject PlayerMesh;

    public AudioSource GetGoodItemSound;
    public AudioSource GetBadItemSound;
    public AudioSource ExplosionSE;

    
    public Animator PlayerAnimator;


    //今いる時間と場所
    public struct _FirstPoint
    {
        public int m_FlagTime;
        public int m_FlagPosID;
    }

    //次行く時間と場所
    struct _SecondPoint
    {
        public int m_FlagTime;
        public int m_FlagPosID;
    }


    void Start()
    {
        m_rig2D = GetComponent<Rigidbody2D>();
        transform.position = new Vector3(GameManager.m_SpownPos[0], GameManager.m_GROUND + 0.5f, 0);

        //待機アニメーション(歩いてない時)
        PlayerAnimator.GetComponentInChildren<Animator>();
        PlayerAnimator.SetBool("IsWalk", false);
    }

    //コルーチン開始用
    public void ColStart()
    {
        //Debug.Log("ColStart");
        //コルーチン開始
        StartCoroutine(PlayerWalkCol(0, 0));
    }

    /// <summary>
    /// タイムラインの値と同期して旗の場所へ移動
    /// </summary>
    IEnumerator PlayerWalkCol(int FirstTime,int FirstPosID)
    {
        //歩くアニメーション
        PlayerAnimator.GetComponentInChildren<Animator>();
        PlayerAnimator.SetBool("IsWalk", true);


        _FirstPoint firstP;
        _SecondPoint secondP;
        //1つ目の旗
        firstP.m_FlagTime = FirstTime;
        firstP.m_FlagPosID = FirstPosID;
        //2つ目の旗(初期値：1つ目の旗と同じポジション)
        secondP.m_FlagTime = TimeLine.MaxTime;
        secondP.m_FlagPosID = firstP.m_FlagPosID;

        //MaxTimeまでのタイムライン上に旗があるか判断
        for (int i = firstP.m_FlagTime+1; i <= TimeLine.MaxTime; i++)
        {
            //i秒目に旗が置いてあった場合(FlagPosIDが - 1以外)
            if (FlagGroup.Instance.SliderTime[i].FlagPosID != -1)
            {
                Debug.Log("Flag!" + FlagGroup.Instance.SliderTime[i].FlagPosID);
                secondP.m_FlagTime = i;
                secondP.m_FlagPosID = FlagGroup.Instance.SliderTime[i].FlagPosID;
                break;//for文を抜ける
            }
        }

        while (TimeLine.Instance.time < secondP.m_FlagTime)
        {
            m_rig2D.MovePosition(
                new Vector2(
                    //X軸は今の時間にいるべき場所
                    Mathf.Lerp(
                        GameManager.m_SpownPos[firstP.m_FlagPosID],
                        GameManager.m_SpownPos[secondP.m_FlagPosID],
                        ((TimeLine.Instance.time - firstP.m_FlagTime) / (secondP.m_FlagTime - firstP.m_FlagTime))
                     ),
                  //Y軸は地面
                  GameManager.m_GROUND
                )
            );
            //Debug.Log(GameManager.m_SpownPos[secondP.m_FlagPosID]);

            //1フレーム待つ
            yield return null;
        }

        if (TimeLine.Instance.time < TimeLine.MaxTime)
        {
            //コルーチン再帰
            StartCoroutine(PlayerWalkCol(secondP.m_FlagTime, secondP.m_FlagPosID));
        }
    }

    //IEnumerator PlayerWalkCol()
    //{
    //    //1フレーム待つ
    //    yield return null;

    //    while (m_TimeLine.Instance.time < (m_TimeLine.MaxTime + 1))
    //    {
    //        //現在の秒数に旗が置いてあった場合(FlagPosIDが-1以外)
    //        if (FlagGroup.Instance.SliderTime[(int)m_TimeLine.Instance.time].FlagPosID != -1)
    //        {
    //            //現在の秒数においてある旗の場所へ移動
    //            rig2D.MovePosition(new Vector2(GameManager.SpownPos
    //                [
    //                /*現在の秒数のID*/(FlagGroup.Instance.SliderTime
    //                    [
    //                    /*現在の秒数*/(int)m_TimeLine.Instance.time
    //                    ].FlagPosID)
    //                ],

    //                GameManager.Ground));
    //        }
    //        //1フレーム待つ
    //        yield return null;
    //    }
    //}

    /// <summary>
    /// Itemの当たり判定(スコア計算)
    /// </summary>
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Item_Good"))
        {
            //ScoreをPlusScore分足す
            GameManager.m_Score += GameManager.m_PlusScore;
            m_GoodParticle.Emit(20);
            Destroy(collision.gameObject);
            //スコアテキストを同期
            ScoreText.Instance.Start();

            //アイテムゲットサウンド再生
            GetGoodItemSound.Play();
        }
        else if (collision.CompareTag("Item_Bad"))
        {
            //体力を1減らす
            HP.Instance.Damage();
            Destroy(collision.gameObject);

            //アイテムゲットサウンド再生
            GetBadItemSound.Play();

            //体力が0になると爆発して消える(コライダー、子階層のmeshを消して爆発のゲームオブジェクトを表示)
            if (GameManager.m_HP <= 0)
            {
                Debug.Log("Explosion");

                GetComponent<BoxCollider2D>().enabled = false;

                GetComponentInChildren<Animator>().enabled = false;

                PlayerMesh.SetActive(false);

                Fire_Explosion.SetActive(true);

                ExplosionSE.Play();
            }

        }
    }
}
