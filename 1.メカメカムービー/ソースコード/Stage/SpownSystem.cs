﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 登録した時間にオブジェクト生成(MainCamera)
/// </summary>
public class SpownSystem : SingletonMonoBehaviour<SpownSystem>
{
    public GameObject[] StageDataObj;
    //生成させる物のリスト
    private List<SpownItem> _spownObjList = new List<SpownItem>();

    //最後に見たインデックス番号
    public int Header = 0;

    //生成する高さ
    public const float SpownHeight = 8;

    void Start()
    {
        //StageNumが登録してあるステージ数を上回ったらステージ番号を書き換え
        if(GameManager.m_StageNum >= StageDataObj.Length)
        {
            GameManager.m_StageNum = 0;
        }
        //生成させる物のリスト
        _spownObjList = StageDataObj[GameManager.m_StageNum].GetComponent<StageData>().stageData;
    }

    void Update()
    {
        while (_spownObjList.Count > Header && _spownObjList[Header].SpownTime <= TimeLine.Instance.time)
        {
            //生成場所
            Vector3 Pos3d = new Vector3(
                GameManager.m_SpownPos[_spownObjList[Header].PosID],
                SpownHeight
                );

            //生成したオブジェクトに値を渡す
            GameObject item = Instantiate(_spownObjList[Header].Item, Pos3d, Quaternion.identity);
            Item itemScr = item.GetComponent<Item>();
            itemScr.Receive(_spownObjList[Header].SpownTime, _spownObjList[Header].LandingTime);
            
            Header++;
        }
    }
}