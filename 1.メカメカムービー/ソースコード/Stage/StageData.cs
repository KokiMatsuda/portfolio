﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StageData : MonoBehaviour
{
    public int StageNum;

    //生成させる物のリスト
    [SerializeField]
    public List<SpownItem> stageData = new List<SpownItem>();
}

/// <summary>
/// Inspectorに複数データを表示するためのクラス
/// </summary>
[System.Serializable]
public class SpownItem
{

    [Tooltip("生成させるオブジェクト")]
    public GameObject Item;

    [Tooltip("生成場所のインデックス番号"), Range(0, 4)]//生成場所のインデックス番号
    public int PosID;

    [Tooltip("生成時間"), Range(0, TimeLine.MaxTime)]//生成時間
    public int SpownTime;

    [Tooltip("着地時間"), Range(0, TimeLine.MaxTime)]//着地時間
    public int LandingTime;
}