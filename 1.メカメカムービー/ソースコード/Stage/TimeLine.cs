﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 再生時間とText表示(Canvas/m_TimeLine)
/// </summary>
public class TimeLine : SingletonMonoBehaviour<TimeLine>
{

    public Image image;
    
    [Tooltip("時間表示用")]
    public Text timeText;

    //最大再生時間10秒
    public const int MaxTime = 10;

    [Tooltip("現在時間")]
    public float time = 0;

    private void Start()
    {
        timeText.text = ("00" + "       " + "\n / " + MaxTime.ToString());
    }

    public void TimeStart()
    {
        image.fillAmount = 0;
        time = 0;
        StartCoroutine("TimeStartCol");
    }

    /// <summary>
    /// 時間計測＆タイムラインの再生表示
    /// </summary>
    private IEnumerator TimeStartCol()
    {
        while (image.fillAmount < 1)
        {
            image.fillAmount += Time.deltaTime / MaxTime;

            //現在の秒数
            time = image.fillAmount * 10;

            //時間表示
            timeText.text = ((int)time).ToString() + "       " + "\n / " + MaxTime.ToString();

            //1フレーム待つ
            yield return null;

            //3秒待つ
            //yield return new WaitForSeconds(3);
        }

        //1秒待つ
        yield return new WaitForSeconds(1);

        GameManager.Instance.NextMode();
    }
}
