﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialBoardManager : MonoBehaviour
{


    [Tooltip("チュートリアルボードのアニメーション")]
    public Animator m_TutorialBoardAnim;

    public Sprite[] Panels;
    int index = 0;

    public GameObject NextButton;
    public GameObject BackButton;

    void Start()
    {
        StartCoroutine(OpenTutorialBool());

        gameObject.GetComponent<Image>().sprite = Panels[0];
    }

    IEnumerator OpenTutorialBool()
    {
        while (!Scene.FadeInEnd)
        {
            yield return null;
        }
        OpenTutorial();
    }


    void OpenTutorial()
    {
        m_TutorialBoardAnim.SetBool("OpenTutorial", true);
    }

    //チュートリアルを閉じるボタンが押されたとき
    public void TutorialClose()
    {
        //チュートリアルを閉じるアニメーション
        m_TutorialBoardAnim.SetBool("OpenTutorial", false);
    }

    public void NextMode()
    {
        GameManager.Instance.NextMode();
    }


    public void NextPage()
    {
        if (index < Panels.Length-1)
        {
            index++;
            gameObject.GetComponent<Image>().sprite = Panels[index];

            BackButton.SetActive(true);
            
        }
    }

    public void BackPage()
    {
        if (index > 0)
        {
            index--;
            gameObject.GetComponent<Image>().sprite = Panels[index];

            NextButton.SetActive(true);
        }
    }
}